const API_URL = process.env.API_URL
    || 'https://dress-hitlist-backend-iltuscwcpg.now.sh'

const ALGOLIA_APP_ID = process.env.ALGOLIA_APP_ID
    || 'WMT9M93PAC'

const ALGOLIA_API_KEY = process.env.ALGOLIA_API_KEY
    || '8f57c8b66ab6a44bbf931db21cf768bb'


module.exports = {
    API_URL,
    ALGOLIA_APP_ID,
    ALGOLIA_API_KEY,
}