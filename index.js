const algoliasearch = require('algoliasearch')
const got = require('got')
const _ = require('lodash')
const {
    API_URL,
    ALGOLIA_APP_ID,
    ALGOLIA_API_KEY,
} = require('./config')


async function fetchProducts(onPageFetch = _.noop) {
    console.log('[debug]', 'Start fetching all products')
    const perPage = 100
    console.log('[debug]', 'Fetching', perPage, 'per page')
    const firstPage = await got(`${API_URL}/dresses?pageSize=${perPage}&pageNum=1`, { json: true })
    const totalPages = firstPage.body.total_pages
    console.log('[debug]', 'Found', totalPages, 'pages')
    console.log('[debug]', 'Fetching remaining pages...')
    onPageFetch(firstPage.body.items, 1)
    const remainingPages = _.range(1, totalPages-1).map(function (empty, pageIndex) {
        return new Promise((resolve) => {
            const pageNum = pageIndex + 1
            console.log('[debug]', 'Fetching page', pageNum)
            return got(`${API_URL}/dresses?pageSize=${perPage}&pageNum=${pageNum}`, { json: true })
                .then((result) => {
                    console.log('[debug]', 'Page', pageNum, 'fetched successfully with a total of', result.body.items.length, 'items')
                    onPageFetch(result.body.items, pageNum)
                    return resolve(result.body.items)
                }, () => resolve([]))
        })

    })

    const otherPagesItems = _.flatten(await Promise.all(remainingPages))
    const allItems = firstPage.body.items.concat(otherPagesItems)

    console.log('[debug]', 'All pages fetched.', allItems.length, 'items found.')

    return allItems
}

async function fetchProductsDetail(products) {
    console.log('[debug]', 'Fetching products detail')
    return _.flatten(await Promise.all(products.map((product) => {
        return new Promise((resolve) => {
            return got(`${API_URL}/dresses/${product.id}`, { json: true })
                .then((response) => {
                    console.log('[debug]', 'Fetched details for product', product.id)
                    return resolve(Object.assign({}, product, response.body))
                }, () => resolve(product))
        })
    })))
}

async function main() {
    const client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_API_KEY)
    const productsIndex = client.initIndex('products')
    const products = await fetchProducts()
    const productsByChunks = _.chunk(products, 50)

    for (productsChunk of productsByChunks) {
        const detailedProducts = await fetchProductsDetail(productsChunk)
        productsIndex.addObjects(detailedProducts, (err, content) => {
            if(err) {
                console.error(err)
            } else {
                console.log(`[debug] ${productsChunk.length} INDEXED SUCCESSFULlY`)
            }
        })
    }
}

main()